function validateForm () {

    var a = document.forms["myForm"]["customerName"].value;
    if (a == "") 
    {
        alert("Please provide your name.");
        return false;
    }

    var b = document.forms["myForm"]["customerEmail"].value;
    var c = document.forms["myForm"]["customerPhone"].value;
    if (b == "" && c == "") 
    {
        alert("Please provide a way to reach you.");
        return false;
    }

    var d = document.forms["myForm"]["reasons"].value;
    var e = document.forms["myForm"]["additionalInformation"].value;
    if (d == "Other" && e == "")
    {
        alert("Please provide additional information.");
        return false;
    }

    if (
	myForm.myChoices1.checked == false &&
	myForm.myChoices2.checked == false &&
	myForm.myChoices3.checked == false &&
        myForm.myChoices4.checked == false &&
        myForm.myChoices5.checked == false)
    {
        alert("Please select a day to be contacted.");
        return false;
    }

}