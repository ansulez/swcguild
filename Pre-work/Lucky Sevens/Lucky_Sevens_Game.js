function hideResults() {
    document.getElementById("finalTable").style.display = "none";
} 

function play() {
    var userInput = document.getElementById("initialInput").value;
    var bet = userInput;
    var betsArray = [];

        while (bet > 0) {
            var d1 = Math.floor(Math.random() * 6) + 1;
            var d2 = Math.floor(Math.random() * 6) + 1;
            var rolledDice = d1 + d2;
                if(rolledDice != 7) {
                    bet -= 1
                } else { 
                    bet += 4
                }
            betsArray.push(bet)
        }

        var rollTicker = betsArray.length;
        var maxMoney = Math.max.apply(Math, betsArray);
        var rollsAtMaxMoney = betsArray.indexOf(maxMoney);
        var rollsHighestPoint = rollTicker - rollsAtMaxMoney;

        function showResults() {
            document.getElementById("finalTable").style.display = "inline";
            document.getElementById("playButton").innerHTML = "Play Again";
            document.getElementById("initialBet").innerHTML = "$" + userInput +".00";
            document.getElementById("finalrollTicker").innerHTML = rollTicker;
            document.getElementById("finalmaxMoney").innerHTML = "$" + maxMoney + ".00";
            document.getElementById("finalrollsHighestPoint").innerHTML = rollsHighestPoint;
        }

    showResults()
} 